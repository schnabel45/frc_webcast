//object init
$.webcastApp = {};

//testing stuff
$.webcastApp.designMode = false;
$.webcastApp.forceScreen = 0;

//Server Default Address
$.webcastApp.ServerAddress = "http://10.0.100.5/signalr";
$.webcastApp.EnableComm = true;
$.webcastApp.webCastMode = false;

//signalr Logging
$.webcastApp.setLogging = true;

//variables
$.webcastApp.settingsMenu = false;
$.webcastApp.connectionGood = false;
$.webcastApp.fieldStatus = "Uknown";
$.webcastApp.lastFMSComm = 0;

//match object
$.webcastApp.emptyMatch = {}
$.webcastApp.emptyMatch.eventID = "-Event-";
$.webcastApp.emptyMatch.eventCode = "EVNT";
$.webcastApp.emptyMatch.descriptor = "T2-2";
$.webcastApp.emptyMatch.autoTimeRemaining = 10;
$.webcastApp.emptyMatch.autoCompleted = false;
$.webcastApp.emptyMatch.teleTimeRemaining = 230;
$.webcastApp.emptyMatch.timeRemaining = 240;
$.webcastApp.emptyMatch.red1_number = 4444;
$.webcastApp.emptyMatch.red1_rank = 44;
$.webcastApp.emptyMatch.red2_number = 5555;
$.webcastApp.emptyMatch.red2_rank = 55;
$.webcastApp.emptyMatch.red3_number = 6666;
$.webcastApp.emptyMatch.red3_rank = 66;
$.webcastApp.emptyMatch.blue1_number = 1111;
$.webcastApp.emptyMatch.blue1_rank = 11;
$.webcastApp.emptyMatch.blue2_number = 2222;
$.webcastApp.emptyMatch.blue2_rank = 22;
$.webcastApp.emptyMatch.blue3_number = 3333;
$.webcastApp.emptyMatch.blue3_rank = 33;
$.webcastApp.emptyMatch.redAutoLive = 0;
$.webcastApp.emptyMatch.redAutoFinal = 0;
$.webcastApp.emptyMatch.redTeleLive = 0;
$.webcastApp.emptyMatch.redTeleFinal = 0;
$.webcastApp.emptyMatch.redFoulLive = 0;
$.webcastApp.emptyMatch.redFoulFinal = 0;
$.webcastApp.emptyMatch.redScoreLive = 0;
$.webcastApp.emptyMatch.redScoreFinal = 0;
$.webcastApp.emptyMatch.redAssistsLive = 0;
$.webcastApp.emptyMatch.blueAutoLive = 0;
$.webcastApp.emptyMatch.blueAutoFinal = 0;
$.webcastApp.emptyMatch.blueTeleLive = 0;
$.webcastApp.emptyMatch.blueTeleFinal = 0;
$.webcastApp.emptyMatch.blueFoulLive = 0;
$.webcastApp.emptyMatch.blueFoulFinal = 0;
$.webcastApp.emptyMatch.blueScoreLive = 0;
$.webcastApp.emptyMatch.blueScoreFinal = 0;
$.webcastApp.emptyMatch.blueAssistsLive = 0;