$.sounds = {};
$.sounds.chargeSound = new Audio("./webcastSupportFiles/Sounds/CHARGE.wav");
$.sounds.endMatch = new Audio("./webcastSupportFiles/Sounds/ENDMATCH.wav");
$.sounds.foghorn = new Audio("./webcastSupportFiles/Sounds/FOGHORN.wav");
$.sounds.postResults = new Audio("./webcastSupportFiles/Sounds/Logo.wav");
$.sounds.threeBells = new Audio("./webcastSupportFiles/Sounds/three-bells.wav");
$.sounds.timer1Warn = new Audio("./webcastSupportFiles/Sounds/WARNEOM.wav");
$.sounds.fieldReady = new Audio("./webcastSupportFiles/Sounds/FieldReady.wav");

var matchTimer = 0;
var redScore = 0;
var blueScore = 0;

function RegisterHotKey() {
    shortcut.add("Shift+Ctrl+F12", function () {
    	if($.webcastApp.settingsMenu === true){
    		$.webcastApp.screens[7].hide();
            $.webcastApp.settingsMenu = false;
    	} else {
    		$.webcastApp.screens[7].show();
    		$.webcastApp.settingsMenu = true;
    		$.webcastApp.updateSettingsMenu();
    	}
    }, {
        'type': 'keydown',
        'propagate': true,
        'target': document
    });
    shortcut.add("Shift+c", function(){
    	//toggle FMS connection
    	if($.webcastApp.EnableComm === true){
    		$.webcastApp.EnableComm = false;
    		$.webcastApp.toggleCommunications();
    	} else {
    		$.webcastApp.EnableComm = true;
    		$.webcastApp.toggleCommunications();
    	}
    }, {
    	'type': 'keydown',
    	'propagate': true,
    	'target': document
    });
    shortcut.add("Shift+r", function(){
    	$.webcastApp.updateSettingsMenu();
    }, {
    	'type': 'keydown',
    	'propagate': true,
    	'target': document
    });
}

function screenShortcut(int){
    $.setScreen(int);
}

$.setScreen = function(screenNumber){
	var i;
	for(i = 0; i < 8; i++){
		if(i === screenNumber) $.webcastApp.screens[i].show();
		else $.webcastApp.screens[i].hide();
	}
	if(screenNumber === -1) {
		$.webcastApp.screens[0].show();
		$.webcastApp.screens[7].show();
		$.webcastApp.screens.settingsMenu = true;
	}
    $.webcastApp.currentScreen = screenNumber;
}

$.webcastApp.connected = function () {
	$.webcastApp.connectionGood = true;
	$.webcastApp.updateSettingsMenu();
}

$.webcastApp.connectionBroke = function(){
	$.webcastApp.connectionGood = false;
	$.webcastApp.updateSettingsMenu();
}

$.webcastApp.updateSettingsMenu = function(){
	if($.webcastApp.connectionGood === true){
		$("#settings_footer").html("<p>FMS Connection - Online</p>");
	} else {
		$("#settings_footer").html("<p>FMS Connection - Offline</p>");
	}
};

$.webcastApp.updatePrestartScreen = function(){
    $("#previous_match_id").text($.webcastApp.prestartScreen.previousMatch.descriptor);
    $("#red_final_auto").text($.webcastApp.prestartScreen.previousMatch.redAutoFinal);
    $("#red_final_teleop").text($.webcastApp.prestartScreen.previousMatch.redTeleFinal);
    $("#red_final_foul").text($.webcastApp.prestartScreen.previousMatch.redFoulFinal);
    $("#red_final_score").text($.webcastApp.prestartScreen.previousMatch.redScoreFinal);
    $("#blue_final_auto").text($.webcastApp.prestartScreen.previousMatch.blueAutoFinal);
    $("#blue_final_teleop").text($.webcastApp.prestartScreen.previousMatch.blueTeleFinal);
    $("#blue_final_foul").text($.webcastApp.prestartScreen.previousMatch.blueFoulFinal);
    $("#blue_final_score").text($.webcastApp.prestartScreen.previousMatch.blueScoreFinal);
    $("#next_match_id").text($.webcastApp.thisMatch.descriptor);
    $("#red_team_1").text($.webcastApp.thisMatch.red1_number);
    $("#red_team_1_rank").text($.webcastApp.thisMatch.red1_rank);
    $("#red_team_2").text($.webcastApp.thisMatch.red2_number);
    $("#red_team_2_rank").text($.webcastApp.thisMatch.red2_rank);
    $("#red_team_3").text($.webcastApp.thisMatch.red3_number);
    $("#red_team_3_rank").text($.webcastApp.thisMatch.red3_rank);
    $("#blue_team_1").text($.webcastApp.thisMatch.blue1_number);
    $("#blue_team_1_rank").text($.webcastApp.thisMatch.blue1_rank);
    $("#blue_team_2").text($.webcastApp.thisMatch.blue2_number);
    $("#blue_team_2_rank").text($.webcastApp.thisMatch.blue2_rank);
    $("#blue_team_3").text($.webcastApp.thisMatch.blue3_number);
    $("#blue_team_3_rank").text($.webcastApp.thisMatch.blue3_rank);
};

$.webcastApp.updateInMatchScreen = function(){
    $("#live_mode_lbl").text($.webcastApp.fieldStatus);
    $("#inMatch_red3").text($.webcastApp.thisMatch.red3_number);
    $("#inMatch_blue3").text($.webcastApp.thisMatch.blue3_number);
    $("#live_match_number").text($.webcastApp.thisMatch.descriptor);
    $("#inMatch_red2").text($.webcastApp.thisMatch.red2_number);
    $("#inMatch_blue2").text($.webcastApp.thisMatch.blue2_number);
    $("#inMatch_red1").text($.webcastApp.thisMatch.red1_number);
    $("#inMatch_blue1").text($.webcastApp.thisMatch.blue1_number);
    $("#red_live_score").text($.webcastApp.thisMatch.redScoreLive);
    $("#blue_live_score").text($.webcastApp.thisMatch.blueScoreLive);
    $("#red_live_auto").text($.webcastApp.thisMatch.redAutoLive);
    $("#red_live_teleop").text($.webcastApp.thisMatch.redTeleLive);
    $("#red_live_foul").text($.webcastApp.thisMatch.redFoulLive);
    $("#blue_live_auto").text($.webcastApp.thisMatch.blueAutoLive);
    $("#blue_live_teleop").text($.webcastApp.thisMatch.blueTeleLive);
    $("#blue_live_foul").text($.webcastApp.thisMatch.blueFoulLive);
    $("#live_timer").text($.webcastApp.thisMatch.timeRemaining);
    if($.webcastApp.thisMatch.autoCompleted === false){
        tempTime = $.webcastApp.thisMatch.timeRemaining + $.webcastApp.thisMatch.teleTimeRemaining;
    } else {
        tempTime = $.webcastApp.thisMatch.timeRemaining;
    }
    timeBar = (tempTime / ($.webcastApp.thisMatch.autoTimeRemaining + $.webcastApp.thisMatch.teleTimeRemaining)) * 100;
    timeBar = 100 - timeBar;
    $("#timebar").css("width", timeBar + "%");
    $.setScreen(3);
}

$.webcastApp.setAudienceScreen = function(eventInfo, matchInfo){
    $.webcastApp.updateEventInfo(eventInfo);
    $.webcastApp.updateMatchInfo(matchInfo);
    $.webcastApp.thisMatch.redAutoFinal = 0;
    $.webcastApp.thisMatch.redTeleFinal = 0;
    $.webcastApp.thisMatch.redFoulFinal = 0;
    $.webcastApp.thisMatch.redScoreFinal = 0;
    $.webcastApp.thisMatch.redAssistsLive = 0;
    $.webcastApp.thisMatch.blueAutoFinal = 0;
    $.webcastApp.thisMatch.blueTeleFinal = 0;
    $.webcastApp.thisMatch.blueFoulFinal = 0;
    $.webcastApp.thisMatch.blueScoreFinal = 0;
    $.webcastApp.thisMatch.blueAssistsLive = 0;
    $.webcastApp.updatePrestartScreen();
    $.setScreen(2);
    setTimeout(function(){$.setScreen(2)}, 200);
};

$.webcastApp.setInMatchScreen = function(){
    $.webcastApp.updateInMatchScreen();
    $.setScreen(3);
    $.sounds.fieldReady.play();
    $.webcastApp.fieldStatus = "Disabled";
}

$.webcastApp.updateMatchInfo = function(matchInfo){
    $.webcastApp.thisMatch.descriptor = matchInfo.MatchIdentifier;
    $.webcastApp.thisMatch.autoTimeRemaining = matchInfo.AutoStartTime;
    $.webcastApp.thisMatch.teleTimeRemaining = matchInfo.ManualStartTime;
    $.webcastApp.thisMatch.timeRemaining = matchInfo.TimeLeft;
    $.webcastApp.thisMatch.red1_number = matchInfo.Red1TeamId;
    $.webcastApp.thisMatch.red1_rank = matchInfo.Red1CurrentRank;
    $.webcastApp.thisMatch.red2_number = matchInfo.Red2TeamId;
    $.webcastApp.thisMatch.red2_rank = matchInfo.Red2CurrentRank;
    $.webcastApp.thisMatch.red3_number = matchInfo.Red3TeamId;
    $.webcastApp.thisMatch.red3_rank = matchInfo.Red3CurrentRank;
    $.webcastApp.thisMatch.blue1_number = matchInfo.Blue1TeamId;
    $.webcastApp.thisMatch.blue1_rank = matchInfo.Blue1CurrentRank;
    $.webcastApp.thisMatch.blue2_number = matchInfo.Blue2TeamId;
    $.webcastApp.thisMatch.blue2_rank = matchInfo.Blue2CurrentRank;
    $.webcastApp.thisMatch.blue3_number = matchInfo.Blue3TeamId;
    $.webcastApp.thisMatch.blue3_rank = matchInfo.Blue3CurrentRank;
    $.webcastApp.thisMatch.redAutoLive = matchInfo.RedAutonomousScore;
    $.webcastApp.thisMatch.redTeleLive = matchInfo.RedTeleopTotal;
    $.webcastApp.thisMatch.redFoulLive = matchInfo.RedPenalty;
    $.webcastApp.thisMatch.redScoreLive = matchInfo.RedFinalScore;
    $.webcastApp.thisMatch.blueAutoLive = matchInfo.BlueAutonomousScore;
    $.webcastApp.thisMatch.blueTeleLive = matchInfo.BlueTeleopTotal;
    $.webcastApp.thisMatch.blueFoulLive = matchInfo.BluePenalty;
    $.webcastApp.thisMatch.blueScoreLive = matchInfo.BlueFinalScore;
};

$.webcastApp.migrateToFinal = function(matchInfo){
    $.webcastApp.thisMatch.redAutoFinal = matchInfo.RedAutonomousScore;
    $.webcastApp.thisMatch.redTeleFinal = matchInfo.RedTeleopTotal;
    $.webcastApp.thisMatch.redFoulFinal = matchInfo.RedPenalty;
    $.webcastApp.thisMatch.redScoreFinal = matchInfo.RedFinalScore;
    $.webcastApp.thisMatch.blueAutoFinal = matchInfo.BlueAutonomousScore;
    $.webcastApp.thisMatch.blueTeleFinal = matchInfo.BlueTeleopTotal;
    $.webcastApp.thisMatch.blueFoulFinal = matchInfo.BluePenalty;
    $.webcastApp.thisMatch.blueScoreFinal = matchInfo.BlueFinalScore;
    $.webcastApp.prestartScreen.previousMatch = JSON.parse(JSON.stringify($.webcastApp.thisMatch));
    $.webcastApp.updateFinalScore();
};

$.webcastApp.updateFinalScore = function(){
    $("#event_name").text($.webcastApp.thisMatch.eventID);
    $("#final_match_number").text($.webcastApp.thisMatch.descriptor);
    $("#finalScreen_red1").text($.webcastApp.thisMatch.red1_number);
    $("#finalScreen_red2").text($.webcastApp.thisMatch.red2_number);
    $("#finalScreen_red3").text($.webcastApp.thisMatch.red3_number);
    $("#finalScreen_blue1").text($.webcastApp.thisMatch.blue1_number);
    $("#finalScreen_blue2").text($.webcastApp.thisMatch.blue2_number);
    $("#finalScreen_blue3").text($.webcastApp.thisMatch.blue3_number);
    $("#red_auto_final").text($.webcastApp.thisMatch.redAutoFinal);
    $("#red_tele_final").text($.webcastApp.thisMatch.redTeleFinal);
    $("#red_foul_final").text($.webcastApp.thisMatch.redFoulFinal);
    $("#finalScreen_red_final").text($.webcastApp.thisMatch.redScoreFinal);
    $("#blue_auto_final").text($.webcastApp.thisMatch.blueAutoFinal);
    $("#blue_tele_final").text($.webcastApp.thisMatch.blueTeleFinal);
    $("#blue_foul_final").text($.webcastApp.thisMatch.blueFoulFinal);
    $("#finalScreen_blue_final").text($.webcastApp.thisMatch.blueScoreFinal);
};

$.webcastApp.updateEventInfo = function(eventInfo){
    $.webcastApp.thisMatch.eventCode = eventInfo.eventCode;
    $.webcastApp.thisMatch.eventID = eventInfo.Description;
}

$.webcastApp.updateTimer = function(timerInfo){
    $.webcastApp.thisMatch.timeRemaining = timerInfo.Timer;
}

$.webcastApp.updateScore = function(newScore){
    $.webcastApp.thisMatch.redAutoLive = newScore.RedAutonomousScore;
    $.webcastApp.thisMatch.redTeleLive = newScore.RedTeleopScore;
    $.webcastApp.thisMatch.redFoulLive = newScore.RedPenaltyScore;
    $.webcastApp.thisMatch.redScoreLive = newScore.RedFinalScore;
    $.webcastApp.thisMatch.blueAutoLive = newScore.BlueAutonomousScore;
    $.webcastApp.thisMatch.blueTeleLive = newScore.BlueTeleopScore;
    $.webcastApp.thisMatch.blueFoulLive = newScore.BluePenaltyScore;
    $.webcastApp.thisMatch.blueScoreLive = newScore.BlueFinalScore;
    updateInMatchScreen();
}

$.webcastApp.toggleCommunications = function(){
    if($.webcastApp.EnableComm === true){
    	$("#settings_footer").html("<p>FMS Connection - Starting...</p>");
        $.connection.hub.start()
            .done(function () {
            	$.webcastApp.connected();
            })
            .fail(function () {
                $.webcastApp.connectionBroke();
            })
    } else {
    	$("#settings_footer").html("<p>FMS Connection - Stopping...</p>");
    	$.connection.hub.stop();
    	$.webcastApp.connectionBroke();
    }
};

$(function (){
	RegisterHotKey();
	$.connection.hub.logging = $.webcastApp.setLogging;
	var myHub = $.connection.messageServiceHub;
	$.webcastApp.screens = []
	$.webcastApp.screens.push($("#blank_div"));		//screen 6
	$.webcastApp.screens.push($("#logo_div")); 				//screen 0
	$.webcastApp.screens.push($("#pre_match_div"));			//screen 1
	$.webcastApp.screens.push($("#in_match_div"));			//screen 2
	$.webcastApp.screens.push($("#post_match_div"));		//screen 3
	$.webcastApp.screens.push($("#alliance_selection_div"));//screen 4
	$.webcastApp.screens.push($("#bracket_div"));			//screen 5
	$.webcastApp.screens.push($("#settingsMenu"));			//not a screen

	if($.webcastApp.designMode === false){
		$.setScreen($.webcastApp.forceScreen);
	} else {
		$.setScreen(-1);
	}

    $.webcastApp.finalScoreScreen = {};
    $.webcastApp.finalScoreScreen.match = JSON.parse(JSON.stringify($.webcastApp.emptyMatch ));

    $.webcastApp.prestartScreen = {};
    $.webcastApp.prestartScreen.previousMatch = JSON.parse(JSON.stringify($.webcastApp.emptyMatch ));
    $.webcastApp.thisMatch = JSON.parse(JSON.stringify($.webcastApp.emptyMatch ));
    $.webcastApp.updatePrestartScreen();
    $.webcastApp.updateInMatchScreen();

	$.connection.hub.error(function (error) {
        $.webcastApp.connectionBroke();
    });

	/*
	myHub.client.update<item> = function (args){
		do stuff
	};
	*/
	myHub.client.updateVideoSwitchChanged = function(VideoSwitchInfo){
        if(VideoSwitchInfo.Name === "Video Only"){
        	$.setScreen(0);
        } else if(VideoSwitchInfo.Name === "Video With Score"){
            //dono
        } else if(VideoSwitchInfo.Name === "Background"){
            //set screen to ranking scroller
        } else if(VideoSwitchInfo.Name === "Match Result"){
            $.setScreen(4);
        } 
    };

	myHub.client.updatePingAudienceScreen = function(eventID){
		myHub.server.audienceScreenPingResponse(eventID).done(function () {
	        chargeSound.play();
	    }).fail(function (error) {
	        foghorn.play();
	    });
	};

    myHub.client.updateMatchReadyToPreStart = function (match) {
    };

    myHub.client.updateMatchPreStartCompleted = function(match){
    };

    myHub.client.updateSetAudienceScreen = function(eventInfo, matchInfo){
        $.webcastApp.setAudienceScreen(eventInfo, matchInfo);
    };

    myHub.client.updateMatchReady = function(match){
        $.webcastApp.updateMatchInfo(match);
        $.webcastApp.setInMatchScreen();
    };

    myHub.client.updateTimerChanged = function(timerInfo){
        $.webcastApp.updateTimer(timerInfo);
        $.webcastApp.updateInMatchScreen();
    };

    myHub.client.updateScoreChanged = function(score){
        $.webcastApp.updateScore(score);
    };

    myHub.client.updateMatchEndAuto = function(match){
        $.sounds.endMatch.play();
        $("#timebar").css("background-color", "rgb(255, 0, 0)");
        $.webcastApp.fieldStatus = "Disabled";
        $.webcastApp.updateMatchInfo(match);
        $.webcastApp.updateInMatchScreen();
        $.webcastApp.thisMatch.autoCompleted = true;
    };

    myHub.client.updateMatchEndTeleop = function(match){
        $.sounds.endMatch.play();
        $("#timebar").css("background-color", "rgb(255, 0, 0)");
        $.webcastApp.fieldStatus = "Disabled";
        $.webcastApp.updateMatchInfo(match);
        $.webcastApp.updateInMatchScreen();
        setTimeout( function(){$.setScreen(0)}, 3000);
    };

    myHub.client.updateMatchPause = function(match){
        $.sounds.foghorn.play();
        $("#timebar").css("background-color", "rgb(255, 0, 0)");
        $.webcastApp.fieldStatus = "Disabled";
        $.webcastApp.updateMatchInfo(match);
        $.webcastApp.updateInMatchScreen();
    };

    myHub.client.updateMatchPostResults = function(match){
        $.sounds.postResults.play();
        $.webcastApp.updateMatchInfo(match);
        $.webcastApp.migrateToFinal(match);
        setTimeout( function(){$.setScreen(4)}, 1000);
    };

    myHub.client.updateMatchStartAuto = function(match){
        $.sounds.chargeSound.play();
        $("#timebar").css("background-color", "rgb(0, 255, 0)");
        $.webcastApp.fieldStatus = "Auto";
        $.webcastApp.updateMatchInfo(match);
        $.webcastApp.updateInMatchScreen();
    };

    myHub.client.updateMatchStartTeleop = function(match){
        $.sounds.endMatch.pause();
        $.sounds.endMatch.currentTime = 0;
        $.sounds.threeBells.play();
        $("#timebar").css("background-color", "rgb(0, 255, 0)");
        $.webcastApp.fieldStatus = "Teleoperated";
        $.webcastApp.updateMatchInfo(match);
        $.webcastApp.updateInMatchScreen();
    };

    myHub.client.updateMatchTimerWarning1 = function(){
        $.sounds.timer1Warn.play();
        $("#timebar").css("background-color", "rgb(255, 255, 0)");
    };

    myHub.client.updateMatchTimerWarning2 = function(){
    }

    myHub.client.updatePreStartInitiated = function(match){
        $.webcastApp.previousMatch = JSON.parse(JSON.stringify($.webcastApp.thisMatch));
        $.webcastApp.thisMatch = JSON.parse(JSON.stringify($.webcastApp.thisMatch));
    }

    $.connection.hub.disconnected(function () {
        setTimeout(function () {
        	if($.webcastApp.EnableComm === true){
            	$.connection.hub.start();
            } else {
            	console.log("Connection Stopped");
            }
        }, 500); // Restart connection after 5 seconds.
    });

    if($.webcastApp.EnableComm === true){
        $.connection.hub.start()
            .done(function () {
            	$.webcastApp.connected();
            })
            .fail(function () {
                $.webcastApp.connectionBroke();
            })
    } else {
    	$.webcastApp.connectionBroke();
    }
});
