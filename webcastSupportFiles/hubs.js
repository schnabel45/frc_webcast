/*!
 * ASP.NET SignalR JavaScript Library v1.0.0
 * http://signalr.net/
 *
 * Copyright Microsoft Open Technologies, Inc. All rights reserved.
 * Licensed under the Apache 2.0
 * https://github.com/SignalR/SignalR/blob/master/LICENSE.md
 *
 */

/// <reference path="..\..\SignalR.Client.JS\Scripts\jquery-1.6.4.js" />
/// <reference path="jquery.signalR.js" />
(function ($, window) {
    /// <param name="$" type="jQuery" />
    "use strict";

    if (typeof ($.signalR) !== "function") {
        throw new Error("SignalR: SignalR is not loaded. Please ensure jquery.signalR-x.js is referenced before ~/signalr/hubs.");
    }

    var signalR = $.signalR;

    function makeProxyCallback(hub, callback) {
        return function () {
            // Call the client hub method
            callback.apply(hub, $.makeArray(arguments));
        };
    }

    function registerHubProxies(instance, shouldSubscribe) {
        var key, hub, memberKey, memberValue, subscriptionMethod;

        for (key in instance) {
            if (instance.hasOwnProperty(key)) {
                hub = instance[key];

                if (!(hub.hubName)) {
                    // Not a client hub
                    continue;
                }

                if (shouldSubscribe) {
                    // We want to subscribe to the hub events
                    subscriptionMethod = hub.on;
                }
                else {
                    // We want to unsubscribe from the hub events
                    subscriptionMethod = hub.off;
                }

                // Loop through all members on the hub and find client hub functions to subscribe/unsubscribe
                for (memberKey in hub.client) {
                    if (hub.client.hasOwnProperty(memberKey)) {
                        memberValue = hub.client[memberKey];

                        if (!$.isFunction(memberValue)) {
                            // Not a client hub function
                            continue;
                        }
                        
                        subscriptionMethod.call(hub, memberKey, makeProxyCallback(hub, memberValue));
                    }
                }
            }
        }
    }

    signalR.hub = $.hubConnection($.webcastApp.ServerAddress, { useDefaultPath: false })
        .starting(function () {
            // Register the hub proxies as subscribed
            // (instance, shouldSubscribe)
            registerHubProxies(signalR, true);

            this._registerSubscribedHubs();
        }).disconnected(function () {
            // Unsubscribe all hub proxies when we "disconnect".  This is to ensure that we do not re-add functional call backs.
            // (instance, shouldSubscribe)
            registerHubProxies(signalR, false);
        });

    signalR.messageServiceHub = signalR.hub.createHubProxy('messageServiceHub'); 
    signalR.messageServiceHub.client = { };
    signalR.messageServiceHub.server = {
        alert: function (alert) {
            /// <summary>Calls the Alert method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"alert\" type=\"Object\">Server side type is FMS.Contract.AlertData</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["Alert"], $.makeArray(arguments)));
         },

        allianceChanged: function (allianceInfo) {
            /// <summary>Calls the AllianceChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"allianceInfo\" type=\"Object\">Server side type is FMS.Contract.AlliancePairingInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["AllianceChanged"], $.makeArray(arguments)));
         },

        audienceScreenPingResponse: function (eventId) {
            /// <summary>Calls the AudienceScreenPingResponse method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"eventId\" type=\"Number\">Server side type is System.Int32</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["AudienceScreenPingResponse"], $.makeArray(arguments)));
         },

        backupCompleted: function (backup) {
            /// <summary>Calls the BackupCompleted method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"backup\" type=\"Object\">Server side type is FMS.Contract.BackupData</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["BackupCompleted"], $.makeArray(arguments)));
         },

        backupStarted: function (backup) {
            /// <summary>Calls the BackupStarted method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"backup\" type=\"Object\">Server side type is FMS.Contract.BackupData</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["BackupStarted"], $.makeArray(arguments)));
         },

        bracketUpdated: function (bracketInfo) {
            /// <summary>Calls the BracketUpdated method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"bracketInfo\" type=\"Object\">Server side type is FMS.Contract.BracketInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["BracketUpdated"], $.makeArray(arguments)));
         },

        dSControl: function (control) {
            /// <summary>Calls the DSControl method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"control\" type=\"Object\">Server side type is FMS.Contract.DSControlInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["DSControl"], $.makeArray(arguments)));
         },

        dSToFMSStatus: function (status) {
            /// <summary>Calls the DSToFMSStatus method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"status\" type=\"Object\">Server side type is FMS.Contract.DSToFMSStatusInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["DSToFMSStatus"], $.makeArray(arguments)));
         },

        eStopChanged: function (estopinfo) {
            /// <summary>Calls the EStopChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"estopinfo\" type=\"Object\">Server side type is FMS.Contract.EStopInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["EStopChanged"], $.makeArray(arguments)));
         },

        fieldNetworkStatus: function (utilInfo) {
            /// <summary>Calls the FieldNetworkStatus method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"utilInfo\" type=\"Object\">Server side type is System.Collections.Generic.List`1[FMS.Contract.FieldNetworkStatusInfo]</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["FieldNetworkStatus"], $.makeArray(arguments)));
         },

        getScoreGoalsState: function () {
            /// <summary>Calls the GetScoreGoalsState method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["GetScoreGoalsState"], $.makeArray(arguments)));
         },

        getVideoSwitchState: function () {
            /// <summary>Calls the GetVideoSwitchState method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["GetVideoSwitchState"], $.makeArray(arguments)));
         },

        matchChanged: function (eventInfo, matchInfo) {
            /// <summary>Calls the MatchChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"eventInfo\" type=\"Object\">Server side type is FMS.Contract.MessagingFIRSTEventInfo</param>
            /// <param name=\"matchInfo\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchChanged"], $.makeArray(arguments)));
         },

        matchCommit: function () {
            /// <summary>Calls the MatchCommit method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchCommit"], $.makeArray(arguments)));
         },

        matchEndAuto: function (match) {
            /// <summary>Calls the MatchEndAuto method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchEndAuto"], $.makeArray(arguments)));
         },

        matchEndTeleop: function (match) {
            /// <summary>Calls the MatchEndTeleop method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchEndTeleop"], $.makeArray(arguments)));
         },

        matchNotReady: function () {
            /// <summary>Calls the MatchNotReady method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchNotReady"], $.makeArray(arguments)));
         },

        matchPauseAuto: function (match) {
            /// <summary>Calls the MatchPauseAuto method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchPauseAuto"], $.makeArray(arguments)));
         },

        matchPauseTeleop: function (match) {
            /// <summary>Calls the MatchPauseTeleop method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchPauseTeleop"], $.makeArray(arguments)));
         },

        matchPlayStatus: function (matchPlayStatusInfo) {
            /// <summary>Calls the MatchPlayStatus method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"matchPlayStatusInfo\" type=\"Object\">Server side type is FMS.Contract.MatchPlayStatusInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchPlayStatus"], $.makeArray(arguments)));
         },

        matchPostResults: function (match) {
            /// <summary>Calls the MatchPostResults method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchPostResults"], $.makeArray(arguments)));
         },

        matchPreStartCompleted: function (match) {
            /// <summary>Calls the MatchPreStartCompleted method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchPreStartCompleted"], $.makeArray(arguments)));
         },

        matchPreStartInitiated: function (match) {
            /// <summary>Calls the MatchPreStartInitiated method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchPreStartInitiated"], $.makeArray(arguments)));
         },

        matchReady: function (match) {
            /// <summary>Calls the MatchReady method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchReady"], $.makeArray(arguments)));
         },

        matchReadyToPreStart: function (match) {
            /// <summary>Calls the MatchReadyToPreStart method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchReadyToPreStart"], $.makeArray(arguments)));
         },

        matchStartAuto: function (match) {
            /// <summary>Calls the MatchStartAuto method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchStartAuto"], $.makeArray(arguments)));
         },

        matchStartTeleop: function (match) {
            /// <summary>Calls the MatchStartTeleop method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"match\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchStartTeleop"], $.makeArray(arguments)));
         },

        matchTimerWarning1: function () {
            /// <summary>Calls the MatchTimerWarning1 method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchTimerWarning1"], $.makeArray(arguments)));
         },

        matchTimerWarning2: function () {
            /// <summary>Calls the MatchTimerWarning2 method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["MatchTimerWarning2"], $.makeArray(arguments)));
         },

        pingAudienceScreen: function (eventId) {
            /// <summary>Calls the PingAudienceScreen method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"eventId\" type=\"Number\">Server side type is System.Int32</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["PingAudienceScreen"], $.makeArray(arguments)));
         },

        pingPitScreen: function (eventId) {
            /// <summary>Calls the PingPitScreen method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"eventId\" type=\"Number\">Server side type is System.Int32</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["PingPitScreen"], $.makeArray(arguments)));
         },

        pitScreenPingResponse: function (eventId) {
            /// <summary>Calls the PitScreenPingResponse method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"eventId\" type=\"Number\">Server side type is System.Int32</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["PitScreenPingResponse"], $.makeArray(arguments)));
         },

        rankChanged: function (rankChangedInfo) {
            /// <summary>Calls the RankChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"rankChangedInfo\" type=\"Object\">Server side type is FMS.Contract.RankChangedInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["RankChanged"], $.makeArray(arguments)));
         },

        scheduleChanged: function (scoreChangedInfo) {
            /// <summary>Calls the ScheduleChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"scoreChangedInfo\" type=\"Object\">Server side type is FMS.Contract.ScheduleChangedInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["ScheduleChanged"], $.makeArray(arguments)));
         },

        scoreChanged: function (score) {
            /// <summary>Calls the ScoreChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"score\" type=\"Object\">Server side type is FMS.Contract.LightWeightScoreInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["ScoreChanged"], $.makeArray(arguments)));
         },

        scoreGoalsChanged: function (status) {
            /// <summary>Calls the ScoreGoalsChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"status\" type=\"Object\">Server side type is FMS.Contract.PLCScoreStatus</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["ScoreGoalsChanged"], $.makeArray(arguments)));
         },

        setAudienceScreen: function (eventInfo, matchInfo) {
            /// <summary>Calls the SetAudienceScreen method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"eventInfo\" type=\"Object\">Server side type is FMS.Contract.MessagingFIRSTEventInfo</param>
            /// <param name=\"matchInfo\" type=\"Object\">Server side type is FMS.Contract.MessagingMatchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["SetAudienceScreen"], $.makeArray(arguments)));
         },

        stationConnectionChanged: function (connectionInfo) {
            /// <summary>Calls the StationConnectionChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"connectionInfo\" type=\"Object\">Server side type is FMS.Contract.StationConnectionInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["StationConnectionChanged"], $.makeArray(arguments)));
         },

        timerChanged: function (timerChangedInfo) {
            /// <summary>Calls the TimerChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"timerChangedInfo\" type=\"Object\">Server side type is FMS.Contract.TimerChangedInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["TimerChanged"], $.makeArray(arguments)));
         },

        tournamentSelectionChanged: function (eventInfo) {
            /// <summary>Calls the TournamentSelectionChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"eventInfo\" type=\"Object\">Server side type is FMS.Contract.MessagingFIRSTEventInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["TournamentSelectionChanged"], $.makeArray(arguments)));
         },

        userAction: function (userAction) {
            /// <summary>Calls the UserAction method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"userAction\" type=\"Object\">Server side type is FMS.Contract.UserActionData</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["UserAction"], $.makeArray(arguments)));
         },

        videoSwitchChanged: function (switchInfo) {
            /// <summary>Calls the VideoSwitchChanged method on the server-side messageServiceHub hub.&#10;Returns a jQuery.Deferred() promise.</summary>
            /// <param name=\"switchInfo\" type=\"Object\">Server side type is FMS.Contract.VideoSwitchInfo</param>
            return signalR.messageServiceHub.invoke.apply(signalR.messageServiceHub, $.merge(["VideoSwitchChanged"], $.makeArray(arguments)));
         }
    };

}(window.jQuery, window));